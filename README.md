# NovelsRecommended

- 评分权重从高到低主要包含以下因素
  + 小说家的天分
  + 剧情的设计
  + 特殊性（时代性，游戏化，meta...）
  + 人物的塑造
  + 小说的完整性，协调性，长度
  + 我对于小说的主观情感

- 星级说明。本星级主要评价小说的优点，星级高的小说并不是没有缺点。没有缺点却很平庸的小说在本评价系统里属于不入流。
  + [⋆⋆⋆⋆⋆] 自成一派，展现了小说家的天分，是独一无二，无法模仿的小说。或许是一个小说家一生里才能写出来一步的小说（基本上一个小说家只会评出一个五星作品）。在大部分因素上都有突出表现，并且缺点并不明显。
  + [⋆⋆⋆⋆] 接近神作，但是因为这样那样的原因差了一步。有很强的个人风格，他人难以模仿。在大部分因素上都有突出表现。
  + [⋆⋆⋆] 一般天才的作品，让人感叹作者的才华，却也不会那么吃惊。在一些因素上有突出表现，但是也有很多缺点。可以被当成作者的成熟形态，能否更进一步无法预测。
  + [⋆⋆] 让人眼前一亮的作品，可以发现作者的独特性。至少在某一个方面十分突出，期待以后可以继续写出更好的小说。
  + [⋆] 值得花时间一读的小说。在某个方面让人觉得眼前一亮。

- 其他说明：
  + 有部分小说我是很多年前读的，记得不是很清楚了，分数可能不符合我的标准。

##### 2019

- [⋆⋆⋆⋆⋆] 发条精灵战记：天镜的极北之星 (宇野朴人)
- [⋆⋆⋆] 被学生威胁能算是犯罪吗 (相乐总)
- [⋆⋆] 西野～校内地位最底层的异能世界最强少年～ (ぶんころり)
- [⋆⋆⋆] 将七柄魔剑支配 (宇野朴人)
- [⋆] 加入年轻人敬而远之的黑魔法公司，没想到工作待遇好，社长和使魔也超可爱，太棒了！(森田季节)
- [⋆] 被死神抚养的少女怀抱漆黑之剑 (彩峰舞人)
- [⋆] 我们的重制人生 (木绪なち)
- [⋆⋆] 女骑士小姐，我们去血拼吧！ (伊藤ヒロ)
- [⋆] 对魔导学园35试验小队 (柳实冬贵)
- [⋆] 带着外挂转生为公会柜台小姐 (夏にコタツ)
- [⋆⋆⋆⋆⋆] 猫耳猫（只有我知道这个世界是个游戏）(ウスバー)
- [⋆⋆] 重来吧、魔王大人！(神埼黑音)
- [⋆] 练好练满！用寄生外挂改造尼特人生！？(伊垣久大)
- [⋆⋆⋆] 死党角色很难当吗？(伊达康)
- [⋆⋆] 这个勇者明明超TUEEE却过度谨慎 (土日月)
- [⋆⋆⋆⋆] 龙王的工作！(白鸟士郎)
- [⋆⋆⋆⋆] 农林 (白鸟士郎)
- [⋆⋆⋆] 异世界狂想曲 (爱七ひろ)
- [⋆] 周末冒险者 (るうせん)
- [⋆⋆⋆⋆] 0能者九条凑 (叶山透)
- [⋆⋆⋆] OVERLORD (丸山くがね)
- [⋆⋆⋆] 29与JK ～社令难违牵手女高中生～ (裕时悠示)
- [⋆] 谁都可以暗中助攻讨伐魔王 (槻影)
- [⋆] 最后的救世主 (岩井恭平)
- [⋆⋆] 想要成为影之实力者 (逢泽大介)
- [⋆⋆⋆] 幼女战记 (カルロ・ゼン)
- [⋆⋆] 我是蜘蛛,怎么了（不过是蜘蛛什么的）(马场翁)
- [⋆] 剃须。然后捡到女高中生 (しめさば)
- [⋆] 7th (???)
- [⋆⋆⋆⋆⋆] 谦虚踏实的生活下去吧！(ひよこのケーキ)
- [⋆⋆⋆] GAMERS! (葵关南)
- [⋆] 喜欢本大爷的竟然就你一个？(骆驼)
- [⋆⋆⋆⋆⋆] 虫之歌 (岩井恭平)
- [⋆⋆⋆⋆] 灰与幻想的格林姆迦尔 (十文字青)
- [⋆] 异世界迷宫最深部为目标 (割内@タリサ)
- [⋆⋆] 为美好的世界献上祝福！(晓夏目)
- [⋆⋆] 结束时在做什么呢？正忙着吗？被拯救可以吗？(枯野瑛)
- [⋆] 谁说尼特族在异世界就会认真工作? (刈野ミカタ)
- [⋆⋆⋆] Campione！弑神者! (丈月城)
- [⋆⋆] 剑神的继承者 镜游)
- [⋆⋆] 来到异世界迷宫都市的我成了治愈魔法师 (幼驯じみ)
- [⋆⋆⋆] 断章格林童话 (甲田学人)
- [⋆⋆⋆⋆⋆] 邪神大沼 (川岸殴鱼)
- [⋆⋆⋆⋆] 蔷薇的玛利亚 (十文字青)
- [⋆⋆] 野生的最终boss出现了 (炎头)
- [⋆⋆⋆⋆] 逃杀竞技场 (土桥真二郎)
- [⋆⋆⋆⋆⋆] 绫辻行人馆系列 (绫辻行人)
- [⋆⋆⋆⋆] 我的青春恋爱物语果然有问题。 (渡航)
- [⋆⋆] 要是有个妹妹就好了 (平坂读)
- [⋆⋆⋆] 我女友与青梅竹马的惨烈修罗场 (裕时悠示)
- [⋆] 田中的异世界称霸 (ぐり)
- [⋆] 月光下的异世界之旅 (ぁずみ圭)
- [⋆⋆⋆] 怀抱火轮的少女 (七泽またりが)
- [⋆] 诸神的差使 (浅叶夏)
- [⋆] 可喜可贺，我进化成了美少女 (久井透夏)
- [⋆⋆⋆⋆] 文学少女 (野村美月)
- [⋆⋆⋆] 海盗女王 (皆川博子)
- [⋆⋆⋆] 梦沉抹大拉 (支仓冻砂)
- [⋆⋆⋆] 六花之勇者 (山形石雄)
- [⋆⋆] 姐姐是中二 地上最强弟弟!? (藤孝刚志)
- [⋆⋆] 野猫玛利 (十文字青)
- [⋆⋆⋆] 神兵编年史 Chronicle Legion (丈月城)
- [⋆⋆⋆⋆] 翼之归处 (妹尾由布子)
- [⋆⋆⋆] 甘城辉煌乐园救世主 (贺东招二)
- [⋆⋆⋆] 碧阳学园学生会议事录 (葵关南)
- [⋆⋆⋆] 时钟机关之星 (榎宮祐 & 暇奈椿)
- [⋆⋆⋆⋆⋆] 我的生存意义 (赤月驱矢)
- [⋆⋆⋆] 东京侵域：Closed Eden (岩井恭平)
- [⋆⋆] EX！(织田兄第)
- [⋆⋆⋆⋆] 永生之酒 (成田良悟)
- [⋆] APRICOT RED 变态艺闻录 (北国ばらっど)
- [⋆⋆⋆⋆] 舞星洒落的雷涅席库尔 (裕时悠示)
- [⋆⋆⋆] 愧疚游戏的制作方法 (荒川工)
- [⋆] 萝球社！(苍山探)
- [⋆⋆] 时槻风乃与暗黑童话之夜 (甲田学人)
- [⋆] 政宗君的复仇 (竹冈叶月)
- [⋆⋆] 寄生女友佐奈 (砂义出云)
- [⋆⋆⋆] 赤村崎葵子的分析是瞎扯淡 (十阶堂一系)
- [⋆] 终焉之栞 (スズム)
- [⋆⋆⋆⋆⋆] 来自新世界 (貴志祐介)
- [⋆⋆⋆] 红Kure-Nai (片山宪太郎)
- [⋆⋆] 魔王之类的啦！(原田源五郎)
- [⋆⋆⋆] Another (绫辻行人)
- [⋆⋆] 女性向游戏攻略对象竟是我…！？ (秋目人)
- [⋆⋆⋆] 国王游戏 (金泽伸明)
- [⋆⋆] 肌肉之神马思鲁 (佐藤ケイ)
- [⋆⋆⋆⋆⋆] SHI-NO黑魂少女 (上月雨音)
- [⋆⋆] 辉夜魔王式! (月见草平)
- [⋆⋆⋆] 女生人气妹和受难的我 (夏绿)
- [⋆⋆] 我的妹妹会读汉字 (梶井孝)
- [⋆⋆] 丧女会的不当日常 (海冬零儿)
- [⋆⋆⋆] 消遣的挑战者 (岩井恭平)
- [⋆] 女高中生店长的便利店真不轻松啊 (明坂缀里)
- [⋆⋆⋆] 少女大神!!!!! (比嘉智康)
- [⋆⋆⋆] 吸血鬼猎人D (菊地秀行)
- [⋆] 推理要在晚餐后 (东川笃哉)
- [⋆] 他与食人者的日常 (火海坂猫)
- [⋆⋆⋆] 空色感染爆发 (本田诚)
- [⋆⋆⋆] 神灯女仆 (夏绿)
- [⋆⋆⋆] 药师寺凉子怪奇事件簿 (田中芳树)
- [⋆⋆⋆⋆] 传说中勇者的传说 (镜贵也)
- [⋆⋆⋆⋆⋆] 428 〜被封鎖的涉谷〜 (石田晶)
- [⋆⋆⋆⋆] 天地明察 (沖方丁)
- [⋆⋆⋆⋆] 逃出乐园岛 (土桥真二郎)
- [⋆] 奋斗吧！系统工程师 (夏海公司)
- [⋆⋆⋆⋆⋆] 9S 遗产尖兵 (叶山透)
- [⋆⋆] 原点回归walkers (森田季节)
- [⋆⋆⋆⋆] 变态王子与不笑猫 (相乐总)
- [⋆⋆⋆⋆] 魔法禁书目录 (镰池和马)
- [⋆⋆⋆] 虚轴少女 (藤原佑)
- [⋆⋆⋆] 临界杀机 (神崎紫电)
- [⋆⋆⋆] 恶魔同盟 (うえお久光)
- [⋆⋆⋆] 世界的中心、针山先生 (成田良悟)
- [⋆⋆⋆⋆⋆] DDD (奈须蘑菇)
- [⋆⋆⋆⋆⋆] 扑杀天使 (おかゆまさき)
- [⋆⋆⋆⋆] 紫色的Qualia (うえお久光)
- [⋆⋆⋆⋆] 暗黑童话 (乙一)
- [⋆⋆⋆⋆] 扉之外 (土桥真二郎)
- [⋆⋆] 闲狼作家是美少女妖怪 (杉井光)
- [⋆⋆⋆] 万岁系列 (三浦勇雄)
- [⋆⋆] 青叶君与宇宙人 (松野秋鸣)
- [⋆⋆⋆⋆] PSYCHE (唐边叶介 = 濑户口廉也)
- [⋆⋆⋆] 魔法使之夜 (奈须蘑菇)
- [⋆⋆⋆⋆⋆] 空之境界 (奈须蘑菇)
- [⋆⋆] 佳奈～妹妹～ (田中罗密欧)
- [⋆⋆⋆⋆] 电波系彼女 (片山宪太郎)
